# **This project is used for the Coursera Full Stack Specialization Capstone. The author is Peter Ho. No plagiarism is allowed!** #

# full-stack-capstone

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.16.0.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.