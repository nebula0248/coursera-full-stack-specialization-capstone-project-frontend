'use strict';

/**
 * @ngdoc overview
 * @name UsedBookMarketplaceApp
 * @description
 * # fullStackCapstoneApp
 *
 * Main module of the application.
 */
angular
  .module('UsedBookMarketplaceApp', [
    'ui.router',
    'ngResource',
    'config'
  ])

  .config(function($stateProvider, $urlRouterProvider, $locationProvider) {
    $locationProvider.hashPrefix('');

    $stateProvider
      .state('app', {
        url: '/',
        views: {
          'header': {
            templateUrl: 'views/header.html'
          },
          'footer': {
            templateUrl: 'views/footer.html'
          },
          'content': {
            templateUrl: 'views/main.html',
            controller: 'MainController'
          }
        },
        params: {
          isBookCreated: false
        }
      })  // End state('app')

      .state('app.bookcreate', {
        url: 'book/create',
        views: {
          'content@': {
            templateUrl: 'views/book_creation.html',
            controller: 'BookCreationController'
          }
        }
      })  // End state('app.bookcreate')

      .state('app.bookdetails', {
        url: 'book/details/:id',
        views: {
          'content@': {
            templateUrl: 'views/book_details.html',
            controller: 'BookDetailsController'
          }
        }
      })  // End state('app.bookdetails')

      .state('app.shoppingcart', {
        url: 'cart/',
        views: {
          'content@': {
            templateUrl: 'views/shopping_cart.html',
            controller: 'ShoppingCartController'
          }
        }
      });  // End state('app.shoppingcart')

    $urlRouterProvider.otherwise('/');
  });  // End config
