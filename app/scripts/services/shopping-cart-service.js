'use strict';

angular.module('UsedBookMarketplaceApp').service('shoppingCartService', ['$window',
  function($window) {
    var localStorageKey = "cartBooks";

    if(!! $window.localStorage[localStorageKey] == false) {
      $window.localStorage[localStorageKey] = JSON.stringify([]);
    }

    this.addBook = function(bookId) {
      var bookArray = this.getAllBooks();

      if(bookArray.indexOf(bookId) != -1) {
        return;
      }

      bookArray.push(bookId);
      $window.localStorage[localStorageKey] = JSON.stringify(bookArray);
    };

    this.removeBook = function(bookId) {
      var bookArray = this.getAllBooks();
      bookArray = removeArrayElement(bookArray, bookId);
      $window.localStorage[localStorageKey] = JSON.stringify(bookArray);
    }

    this.getAllBooks = function() {
      return JSON.parse($window.localStorage[localStorageKey]);
    }

    function removeArrayElement(arr) {
      var what, a = arguments, L = a.length, ax;
      while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
          arr.splice(ax, 1);
        }
      }
      return arr;
    }
  }  // End service
]);
