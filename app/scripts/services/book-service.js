'use strict';

angular.module('UsedBookMarketplaceApp').service('bookService', ['$resource', 'ENV', '$http',
  function($resource, ENV, $http) {
    this.getAllBooks = function(successCallback, errorCallback) {
      $resource(ENV.API_URL_BASE + "/books/all", null).query(successCallback, errorCallback);
    };

    this.searchBooksByTitle = function(searchString, successCallback, errorCallback) {
      $resource(ENV.API_URL_BASE + "/books/search/title/:searchString", {
        "searchString": "@searchString"
      }).query({
        "searchString": searchString
      }, successCallback, errorCallback);
    };

    this.getBookById = function(id, successCallback, errorCallback) {
      $resource(ENV.API_URL_BASE + "/books/get/:id", {
        "id": "@id"
      }).get({
        "id": id
      }, successCallback, errorCallback);
    };

    this.saveNewBook = function(formInput, successCallback, errorCallback) {
      var formData = new FormData();

      formData.append("image", formInput.image);
      delete formInput.image;

      for (var key in formInput) {
        formData.append(key, formInput[key]);
      }

      $http.post(ENV.API_URL_BASE + "/books/create", formData, {
        withCredentials : false,
        headers : {
          'Content-Type' : undefined
        },
        transformRequest : angular.identity
      }).then(successCallback)
        .catch(errorCallback);
    };

    this.saveNewComment = function(bookId, commentObject, successCallback, errorCallback) {
      $http.post(ENV.API_URL_BASE + "/books/comments/create/" + bookId, commentObject)
        .then(successCallback)
        .catch(errorCallback);
    };
  }  // End service
]);
