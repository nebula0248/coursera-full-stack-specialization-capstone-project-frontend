'use strict';

angular.module('UsedBookMarketplaceApp').controller('ShoppingCartController', ['$scope', 'bookService', 'shoppingCartService',
  function ($scope, bookService, shoppingCartService) {
    $scope.showCheckoutModal = false;

    // Constructor
    (function() {
      loadBooks();
    })();

    $scope.removeBook = function(bookId) {
      shoppingCartService.removeBook(bookId);
      loadBooks();
    };

    $scope.displayCheckoutModal = function() {
      $scope.showCheckoutModal = true;
    };

    function loadBooks() {
      $scope.shoppingCartBooks = [];
      $scope.totalPrice = 0.0;

      bookService.getAllBooks(function(response) {
        var allBooks = response;
        var allShoppingCarBookIds = shoppingCartService.getAllBooks();

        allBooks.forEach(function(book, index) {
          if(allShoppingCarBookIds.indexOf(book._id) != -1) {
            $scope.shoppingCartBooks.push(book);
            $scope.totalPrice += parseFloat(book.price);
          }
        });
      }, function(error) {
        console.error(error);
      });
    }
  }  // End controller
]);
