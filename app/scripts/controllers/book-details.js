'use strict';

angular.module('UsedBookMarketplaceApp').controller('BookDetailsController', ['$scope', 'bookService', 'shoppingCartService', '$stateParams',
  function ($scope, bookService, shoppingCartService, $stateParams) {
    var requestBookId = $stateParams.id;
    var dataReady = false;
    $scope.bookData = {};
    $scope.newCommentData = {};
    $scope.showAddToCartModal = false;
    $scope.showAddedCommentModal = false;

    bookService.getBookById(requestBookId, function(response) {
      $scope.bookData = response;
      dataReady = true;
    }, function(error) {
      console.log("error");
    });

    $scope.addToCart = function() {
      if(! dataReady) {
        return;
      }
      shoppingCartService.addBook($scope.bookData._id);
      $scope.showAddToCartModal = true;
    };

    $scope.postNewComment = function() {
      bookService.saveNewComment($scope.bookData._id, $scope.newCommentData, function(response) {
        $scope.bookData.comments.push($scope.newCommentData);
        $scope.newCommentData = {};
        $scope.showAddedCommentModal = true;
      }, function(error) {
        console.log("error");
      });
    };
  }  // End controller
]);
