'use strict';

angular.module('UsedBookMarketplaceApp').controller('BookCreationController', ['$scope', 'bookService', 'ENV', '$state',
  function ($scope, bookService, ENV, $state) {
    $scope.apiBaseURL = ENV.API_URL_BASE;
    $scope.formInput = {
      title: '',
      author: '',
      description: '',
      image: null,
      price: '',
      isbn: '',
      seller: '',
      sellerContact: ''
    };
    $scope.showCreatedBookModal = false;

    $scope.onImageFileSelectionChanged = function(element) {
      $scope.$apply(function($scope) {
        $scope.formInput.image = element.files[0];
      });
    };

    $scope.createNewBook = function() {
      bookService.saveNewBook($scope.formInput, function() {
        $scope.formInput = {
          title: '',
          author: '',
          description: '',
          image: null,
          price: '',
          isbn: '',
          seller: '',
          sellerContact: ''
        };
        $scope.showCreatedBookModal = true;
      }, function(error) {
        console.error(error);
      });
    };
  }  // End controller
]);
