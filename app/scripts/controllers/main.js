'use strict';

angular.module('UsedBookMarketplaceApp').controller('MainController', ['$scope', 'bookService', 'shoppingCartService', '$stateParams',
  function ($scope, bookService, shoppingCartService, $stateParams) {
    var dataReady = false;
    $scope.books = [];
    $scope.showAddToCartModal = false;
    $scope.searchBarTitleInput = "";

    // Constructor
    (function() {
      showAllBooks();
    })();

    $scope.addToCart = function(bookId) {
      if(! dataReady) {
        return;
      }
      shoppingCartService.addBook(bookId);
      $scope.showAddToCartModal = true;
    };

    $scope.searchByTitle = function() {
      var input = $scope.searchBarTitleInput;

      if(input.trim() == "") {
        showAllBooks();
        return;
      }

      bookService.searchBooksByTitle(input, function(response) {
        console.log(response);
        $scope.books = response;
        dataReady = true;
      }, function(error) {
        console.error(error);
      })
    };

    $scope.showAllBooks = function() {
      showAllBooks();
    };

    function showAllBooks() {
      $scope.searchBarTitleInput = "";

      bookService.getAllBooks(function(response) {
        $scope.books = response;
        dataReady = true;
      }, function(error) {
        console.error(error);
      });
    }
  }  // End controller
]);
